def translate(string)
piglatin_string = ""
string.split.map {|word| piglatin(word)}.join(" ")
end

def piglatin(word)

vowels = "aieouAIEOU"
letters = word.split("")
consonants_array= []
special_case = "qu"

letters.each_with_index do |letter, index|

if vowels.include?(letter)
   return (letters[index..-1] + consonants_array + "ay".split).join
end
if letters[index] == "q" && letters[index + 1] == "u"
  return (letters[index+2 ..-1] + consonants_array + "qu".split + "ay".split).join
end
consonants_array << letter
end
end

p piglatin("square") #== "aresquay"

# Rule 1: If a word begins with a vowel sound, add an "ay" sound to
# the end of the word.
#
# Rule 2: If a word begins with a consonant sound, move it to the end
# of the word, and then add an "ay" sound to the end of the word.
